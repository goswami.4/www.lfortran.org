---
title: "Fortran On Web Using LFortran"
date: 2024-05-02
tags: ["Fortran", "Announcement", "webassembly", "fortran-lang"]
author: "[Ondřej Čertík](https://ondrejcertik.com/), [Gagandeep Singh](https://github.com/czgdp1807), [Ubaid Shaikh](https://github.com/Shaikh-Ubaid), [Thirumalai Shaktivel](https://www.linkedin.com/in/thirumalai-shaktivel/), [Pranav Goswami](https://www.linkedin.com/in/pranavgoswami1/), [Harshita Kalani](https://github.com/HarshitaKalani), [Parth Mistry](), [Gaurav Dhingra](), [Smit Lunagariya](https://www.linkedin.com/in/smit-lunagariya-356b93179/), [Luthfan Lubis](https://github.com/ansharlubis), [Naman Gera](https://uk.linkedin.com/in/namannimmo), [Zihua Wu](https://github.com/lucifer1004), [Khushi Agrawal](https://khushi-411.github.io/), [Christoph Junghans](https://www.linkedin.com/in/christophjunghans), [Brian Beckman](https://www.linkedin.com/in/brianbeckman), [Dylon Edwards](https://www.linkedin.com/in/dylon-edwards-0936bb39/)"
type: post
draft: false
---


# Fortran On Web Using LFortran

Recently, there was a blog post titled [Fortran on WebAssembly](https://gws.phd/posts/fortran_wasm/) released by [Dr George W Stagg](https://gws.phd/). This article inspired us to compile the same example using our [LFortran](https://lfortran.org/) compiler. We are happy to share that we have the fortran [mnist](https://en.wikipedia.org/wiki/MNIST_database) classifier example used in the blog post, compiled to WebAssembly using LFortran (with no hacks to the compiler) and working perfectly in the browser.


## MNIST

<iframe src="https://lfortran.github.io/mnist-classifier-blas-wasm/" style="width: 100%; height: 282px; border: none;">

</iframe>

We cloned the original authors code and just swapped-in two of our generated files `mnist.js` and `mnist.wasm` with the original files. We also fixed few minor bugs in the code that we came across (details in the commit history).

## Other Examples

Apart from the mnist example, we also have two more examples __2D matrix multiplication__ and __simple linear regression__. The sources of these are present here https://github.com/lfortran/Fortran-On-Web.

<iframe src="https://lfortran.github.io/Fortran-On-Web/matmul2D/" style="width: 100%; height: 354px; border: none;">

</iframe>

<br />

<iframe src="https://lfortran.github.io/Fortran-On-Web/linear-regression/" style="width: 100%; height: 596.83px; border: none;">

</iframe>


## Compiling to wasm using LFortran

LFortran supports compiling to `wasm` via different approaches.

1. Using `clang-wasi`:

```console
lfortran main.f90 --target=wasm32-wasi -o main.wasm

wasmtime main.wasm
```

2. Using `emscripten`:

```console
lfortran main.f90 --target=wasm32-unknown-emscripten -o main.js

node main.js
```


3. Using custom wasm backend:

```console
lfortran main.f90 --backend=wasm -o main

wasmtime main

or

node main.js
```

Out of the above three approaches, using the `clang-wasi` or `emscripten` approach is more stable as it uses our most advanced `llvm` backend.

To use the `clang-wasi` approach, one needs the WASI SDK installed and added to `PATH`. Detailed steps for this are shared [here](https://github.com/lfortran/lfortran/pull/3842#issuecomment-2045610278).

To use the `emscripten` approach, one needs the emscripten SDK (emsdk) installed and added to `PATH`. Detailed steps for this are shared [here](https://github.com/lfortran/lfortran/pull/3877#issuecomment-2059164559).

## Contributing

If you liked the above examples, go ahead and try using LFortran to compile your Fortran codes to WebAssembly. Remember that LFortran is alpha quality, so please report all bugs that you discover. It should be however possible to workaround them in most cases.


We also continuously welcome new contributors to join our endeavor. If you're interested, please reach out to us. Working on a compiler offers a stimulating learning experience, and we're committed to providing all the necessary guidance and training. Join us in shaping the future of LFortran!


## Acknowledgements

We want to thank:

* [Sovereign Tech Fund (STF)](https://sovereigntechfund.de/en/)
* [NumFOCUS](https://numfocus.org/)
* [QuantStack](https://quantstack.net/)
* [Google Summer of Code](https://summerofcode.withgoogle.com/)
* [GSI Technology](https://gsitechnology.com/)
* [LANL](https://lanl.gov/)
* Our GitHub, OpenCollective and NumFOCUS sponsors
* All our contributors (73 so far!)

## Discussions

* Fortran Discourse: https://fortran-lang.discourse.group/t/fortran-on-web-using-lfortran/7957
* Twitter: https://twitter.com/lfortranorg/status/1786129003486581025
